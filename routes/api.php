<?php

use App\Http\Controllers\Api\NewsLetterController;
use App\Http\Controllers\Api\ArticleController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::post('/login',[AuthController::class, 'login']);

Route::group(["middleware" => ["auth:api"]],function(){
    Route::post('categories',[CategoryController::class,'store']);
    Route::match(['put','patch'],'categories/{id}',[CategoryController::class,'update']);
    Route::delete('categories/{id}',[CategoryController::class,'destroy']);

    Route::post('articles',[ArticleController::class,'store']);
    Route::match(['put','patch'],'articles',[ArticleController::class,'update']);
    Route::delete('articles/{id}',[ArticleController::class,'destroy']);
});

Route::get('categories',[CategoryController::class,'index']);
Route::get('categories/{id}',[CategoryController::class,'show']);

Route::get('articles',[ArticleController::class,'index']);
Route::get('articles/{id}',[ArticleController::class,'show']);

Route::post('subscribe',[NewsLetterController::class,'subscribe']);

Route::get('reports',[\App\Http\Controllers\Api\ReportController::class,'report']);
