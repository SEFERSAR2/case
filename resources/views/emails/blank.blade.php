<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

    <!-- START HEADER/BANNER -->

    <tbody>


    <tr>
        <td align="center">
            <table align="center" class="col-600" width="600" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                    <td align="center" bgcolor="#2a3b4c">
                        <table class="col-600" width="600" align="center" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                <td height="33"></td>
                            </tr>
                            <tr>
                                <td>


                                    <table class="col1" width="183" border="0" align="left" cellpadding="0" cellspacing="0">

                                        <tbody><tr>
                                            <td height="18"></td>
                                        </tr>

                                        <tr>
                                            <td align="center">
                                                <img style="display:block; line-height:0px; font-size:0px; border:0px;" class="images_style" src="https://designmodo.com/demo/emailtemplate/images/icon-title.png" alt="img" width="156" height="136">
                                            </td>



                                        </tr>
                                        </tbody></table>



                                    <table class="col3_one" width="380" border="0" align="right" cellpadding="0" cellspacing="0">

                                        <tbody><tr align="left" valign="top">
                                            <td style="font-family: 'Raleway', sans-serif; font-size:20px; color:#f1c40f; line-height:30px; font-weight: bold;">{{ $subject }} </td>
                                        </tr>


                                        <tr>
                                            <td height="5"></td>
                                        </tr>


                                        <tr align="left" valign="top">
                                            <td style="font-family: 'Lato', sans-serif; font-size:14px; color:#fff; line-height:24px; font-weight: 300;">
                                                {{ $content }}
                                            </td>
                                        </tr>

                                        <tr>
                                            <td height="10"></td>
                                        </tr>


                                        </tbody></table>
                                </td>
                            </tr>
                            <tr>
                                <td height="33"></td>
                            </tr>
                            </tbody></table>
                    </td>
                </tr>
                </tbody></table>
        </td>
    </tr>



    </tbody>
</table>
