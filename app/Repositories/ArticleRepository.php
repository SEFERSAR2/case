<?php

namespace App\Repositories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Model;

class ArticleRepository extends AbstractBaseRepository
{
    public function __construct()
    {
        parent::__construct(new Article());
    }

}
