<?php

namespace App\Repositories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;

class CategoryRepository extends AbstractBaseRepository
{
    public function __construct()
    {
        parent::__construct(new Category());
    }
}
