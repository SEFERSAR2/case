<?php

namespace App\Repositories;

use App\Models\Article;
use App\Models\NewsLetter;
use App\Models\ViewArticle;
use Illuminate\Database\Eloquent\Model;

class NewsLetterRepository extends AbstractBaseRepository
{
    public function __construct()
    {
        parent::__construct(new NewsLetter());
    }

}
