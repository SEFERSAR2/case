<?php

namespace App\Http\Controllers\Api;

use App\Helper\ApiHelper;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\NewsLetter;
use App\Models\ViewArticle;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ReportController extends Controller
{
    public function report()
    {
        $now = Carbon::now();
        $reports = [
            'totalArticle' => Article::query()->count('id'),
            'totalSubscribe' => NewsLetter::query()->count('id'),
            'totalViewCount' => ViewArticle::query()->get()->unique('ip')->count(),
            'publishedArticleByThisWeek' => Article::query()->whereBetween('created_at',[
                $now->startOfWeek()->format('Y-m-d'),
                $now->endOfWeek()->format('Y-m-d')
            ])->get()->count(),
            'subscribesByThisMount' => NewsLetter::query()->whereBetween('created_at',[
                $now->startOfMonth()->format('Y-m-d'),
                $now->endOfMonth()->format('Y-m-d')
            ])->get()->count()
        ];

        return ApiHelper::successResponse($reports);
    }
}
