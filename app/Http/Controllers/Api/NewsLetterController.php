<?php

namespace App\Http\Controllers\Api;

use App\Helper\ApiHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsLetterRequest;
use App\Repositories\ContractRepository;
use App\Repositories\NewsLetterRepository;
use Illuminate\Http\Request;

class NewsLetterController extends Controller
{
    private ContractRepository $newsLetterRepository;

    public function __construct()
    {
        $this->newsLetterRepository = new NewsLetterRepository();
    }

    public function subscribe(NewsLetterRequest $request){
        $data = $request->validated();
        $response = $this->newsLetterRepository->create($data);

        return ApiHelper::successResponse($response);

    }
}
