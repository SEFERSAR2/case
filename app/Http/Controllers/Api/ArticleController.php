<?php

namespace App\Http\Controllers\Api;

use App\Helper\ApiHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use App\Repositories\ContractRepository;
use App\Repositories\ViewArticleRepository;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends Controller
{

    private ContractRepository $articleRepository;
    private ContractRepository $viewArticleRepository;

    public function __construct()
    {
        $this->articleRepository     = new ArticleRepository();
        $this->viewArticleRepository = new ViewArticleRepository();
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ApiHelper::successResponse(
            $this
                ->articleRepository
                ->with('category')
                ->withCount('views')
                ->getAllByPaginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ArticleRequest $request)
    {
        $data = $request->validated();
        $data['image'] = 'article_avatar.png';
        $data['owner_id'] = Auth::id();
        if($request->file('image') !== null){
            $data['image'] = $this->fileUpload($request);
        }
        $article = $this->articleRepository->create($data);
        return ApiHelper::successResponse($article);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $article = $this->articleRepository->withCount('views')->with('category')->get($id);
        $this->incrementViewArticleCount($article,$request);
        return ApiHelper::successResponse($article);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ArticleRequest $request,Article $article)
    {
        $data = $request->validated();
        if($request->file('image') !== null){
            if($article->image !== 'article_avatar.png'){
                unlink(public_path('images').'/'.$article->image);
            }
            $data['image'] = $this->fileUpload($request);
        }
        $articleUpdate = $this->articleRepository->update($article->id,$data);
        return ApiHelper::successResponse($articleUpdate);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $article = $this->articleRepository->get($id);
        return ApiHelper::successResponse($article->delete());
    }

    private function fileUpload(ArticleRequest $request){
        $filename = \Illuminate\Support\Str::random().'.'.$request->file('image')->getClientOriginalExtension();
        $request->image->move(public_path('images'),$filename);
        return $filename;
    }

    private function incrementViewArticleCount(Article $article,Request $request){
        $data = [
            'article_id' => $article->id,
            'ip' => $request->getClientIp(),
            'user_id'    => Auth::id()
        ];

        $this->viewArticleRepository->create($data);
    }
}
