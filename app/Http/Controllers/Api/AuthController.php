<?php

namespace App\Http\Controllers\Api;

use App\Helper\ApiHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(AuthRequest $request){
        $data = $request->validated();
        if (!auth()->attempt($data)) {
            return ApiHelper::errorResponse('Invalid Username And Password');
        }
        $token = auth()->user()->createToken('API Token')->accessToken;
        return ApiHelper::successResponse([
            "user" => auth()->user(),
            'access_token' => $token
        ]);
    }
}
