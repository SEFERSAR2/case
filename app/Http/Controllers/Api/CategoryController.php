<?php

namespace App\Http\Controllers\Api;

use App\Helper\ApiHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\ContractRepository;
use Spatie\FlareClient\Api;

class CategoryController extends Controller
{

    private ContractRepository $categoryRepository;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ApiHelper::successResponse(
            $this
                ->categoryRepository
                ->withCount('articles')
                ->getAllByPaginate()
        );
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryRequest $request)
    {
        $data = $request->validated();

        $category = $this->categoryRepository->create($data);

        return ApiHelper::successResponse($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        $category = $this->categoryRepository->withCount('articles')->get($id);

        return ApiHelper::successResponse($category);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CategoryRequest $request, $id)
    {
        $data = $request->validated();
        $updateCategory = $this->categoryRepository->update($id,$data);
        return ApiHelper::successResponse($updateCategory);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = $this->categoryRepository->get($id);
        return ApiHelper::successResponse($category->delete());
    }
}
