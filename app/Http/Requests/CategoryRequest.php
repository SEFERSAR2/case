<?php

namespace App\Http\Requests;

use App\Rules\Slug;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required',$this->uniqueHandle('title')]
        ];
    }

    private function uniqueHandle(string $column): \Illuminate\Validation\Rules\Unique
    {
        if($this->method() === 'PUT' || $this->method() === 'PATCH'){
            $id = $this->route()->parameters()['id'];
            return Rule::unique('categories',$column)->ignore($id);
        }
        return Rule::unique('categories',$column);
    }
}
