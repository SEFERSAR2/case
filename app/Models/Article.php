<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'owner_id',
        'category_id',
        'body',
        'image',
    ];


    public function category(){
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function views(){
        return $this->hasMany(ViewArticle::class,'article_id','id');
    }
}
