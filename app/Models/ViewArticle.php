<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ViewArticle extends Model
{
    use HasFactory;

    protected $table = 'article_views';

    protected $fillable = [
        'article_id',
        'user_id',
        'ip'
    ];
}
