<?php

namespace App\Observers;

use App\Jobs\SubscribeJob;
use App\Mail\NewsLetterEmail;
use App\Models\NewsLetter;
use Illuminate\Support\Facades\Mail;

class NewsLetterObserver
{
    /**
     * Handle the NewsLetter "created" event.
     *
     * @param  \App\Models\NewsLetter  $newsLetter
     * @return void
     */
    public function created(NewsLetter $newsLetter)
    {
        dispatch(new SubscribeJob($newsLetter->email));
    }

    /**
     * Handle the NewsLetter "updated" event.
     *
     * @param  \App\Models\NewsLetter  $newsLetter
     * @return void
     */
    public function updated(NewsLetter $newsLetter)
    {
        //
    }

    /**
     * Handle the NewsLetter "deleted" event.
     *
     * @param  \App\Models\NewsLetter  $newsLetter
     * @return void
     */
    public function deleted(NewsLetter $newsLetter)
    {
        //
    }

    /**
     * Handle the NewsLetter "restored" event.
     *
     * @param  \App\Models\NewsLetter  $newsLetter
     * @return void
     */
    public function restored(NewsLetter $newsLetter)
    {
        //
    }

    /**
     * Handle the NewsLetter "force deleted" event.
     *
     * @param  \App\Models\NewsLetter  $newsLetter
     * @return void
     */
    public function forceDeleted(NewsLetter $newsLetter)
    {
        //
    }
}
