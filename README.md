### Installation:
```
   composer ınstall
```
```
   php artisan migrate
   php artisan passport:install 
```

#### For dummy users:
```
 php artisan db:seed
```

#### Login:
```
    email: test@test.com
    password: password
```

#### Work Job:
```
    php artisan queue:work
```

#### Other:
```
    .env içine mail gönderilebilmesi için mail.trap'dan test mail hesabı açıp api bilgilerini giriniz.
    .env QUEUE_CONNECTION=database olmalı
```
